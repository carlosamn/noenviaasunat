﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoEnviadosASunat
{
    class Document
    {
        public string Ruc { get; set; }
        public string TD { get; set; }
        public string SerieNumero { get; set; }
    }
}
