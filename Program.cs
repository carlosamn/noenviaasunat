﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.Odbc;

namespace NoEnviadosASunat
{
    class Program
    {
        static void Main(string[] args)
        {

            string dir = @"F:\Util\prog\VFPsCPE2\vfp\data\";
            //string dir = @"SunatXML";
            string sfecha1 = "2017-07-26";
            string sfecha2 = "2017-08-06";

            //Recorrer la tabla cpe_doc_cab, facturas, por rango de fechas
            //guardalas en una lista

            var lista = new List<Document>();

            string name = "", user = "", pwd = "", server = "", db = "";
            GetVars(ref name, ref user, ref pwd, ref server, ref db);

            OdbcConnection cn = null;
            try
            {
                cn = GetConnectionFact(name, user, pwd, server, db);
                Console.WriteLine("Se conectó satisfactoriamente!");

                string stringsql = "select rucempresa, tipodocumento, serienumero " +
                    "from cpe_doc_cab where fechaemision between '{0}' and '{1}' " +
                    "and serienumero like 'F%' and ensunat = 1";

                string strquery = string.Format(stringsql, sfecha1, sfecha2);

                var command = new OdbcCommand(strquery, cn);

                var reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        var doc = new Document();
                        doc.Ruc = reader.GetString(0);
                        doc.TD = reader.GetString(1);
                        doc.SerieNumero = reader.GetString(2);
                        lista.Add(doc);
                    }
                }
                finally
                {
                    reader.Close();
                }

                foreach (var item in lista)
                {
                    Console.WriteLine("ruc = {0}, td = {1}, sn = {2}", item.Ruc, item.TD, item.SerieNumero);

                    string nombre = "archiv-ori.xml";



                    string archivo = Path.Combine(dir, nombre);  //dir + "\\" + nombre;
                    if (!File.Exists(archivo))
                    {
                        //No Existe el archivo
                        //update ensunatt=0 encustodia=0 estadoregistro='L'
                    }

                }



            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (cn != null)
                    cn.Close();
            }




            Console.WriteLine("Presione ENTER para continuar...");
            Console.ReadLine();
        }

        private static OdbcConnection GetConnectionFact(string name, string user, string pwd, string server, string db)
        {
            OdbcConnection cn;
            string cx = "Driver={SQL Server};" + string.Format("Name={0};UID={1};PWD={2};Server={3};Database={4};", name, user, pwd, server, db);
            Console.WriteLine(cx);
            cn = new OdbcConnection(cx);
            //cn = new OdbcConnection(string.Format("dsn={0};UID={1};PWD={2};", name, user, pwd));
            cn.Open();
            return cn;
        }

        private static string GetVars(ref string name, ref string user, ref string pwd, ref string server, ref string db)
        {

            var sr = new StreamReader("config.xml", Encoding.Default);
            var config = sr.ReadToEnd();
            var interno = GetByTag(ref config, "ODBC");
            name = GetByTag(ref interno, "NAME");
            user = GetByTag(ref interno, "USER");
            pwd = GetByTag(ref interno, "PWD");
            server = GetByTag(ref interno, "SERVER");
            db = GetByTag(ref interno, "DB");


            return "";

        }

        private static string GetByTag(ref string texto, string tag)
        {
            //var tag = "ODBC";
            var tag1 = "<" + tag + ">";
            var tag2 = "</" + tag + ">";
            var pos = texto.IndexOf(tag1);
            if (pos >= 0)
            {
                var posini = pos + tag1.Length;
                var pos2 = texto.IndexOf(tag2);
                return texto.Substring(posini, pos2 - posini);
            }
            return "";
        }


    }
}
